import Vue from 'vue';
import Vuex from 'vuex'; // vuex es solo para manejar el state de la aplicacion

Vue.use(Vuex);

export default new Vuex.Store({
    state:{

    },
    mutations:{

    },
    getters:{
        
    }
});