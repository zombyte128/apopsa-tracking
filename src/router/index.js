import Vue from 'vue';
import VueRouter from 'vue-router';
import VuePageTransition from 'vue-page-transition';

import IndexComponent from '../components/IndexComponent';

const routes = [
    {
        path:'/',
        component:IndexComponent,
        name:'index'
    }

];

const router = new VueRouter({
    mode: 'history', //es para quita el # que le agrega vue añ user vue router a la url
    routes
});

Vue.use(VueRouter);
Vue.use(VuePageTransition);
export default router;